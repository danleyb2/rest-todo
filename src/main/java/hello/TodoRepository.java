package hello;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;


public interface TodoRepository extends PagingAndSortingRepository<Todo, Long> {
    Optional<Todo> findById(Long id);


}
