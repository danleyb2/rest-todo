package hello;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/todos")
public class TodoController {

	private TodoRepository repository;

	@Autowired
	public TodoController(TodoRepository repository) {
		this.repository = repository;
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Iterable<Todo>> list() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept-Patch", "application/json-patch+json");
		return new ResponseEntity<Iterable<Todo>>(repository.findAll(), headers, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public Todo create(@RequestBody Todo todo) {
		return repository.save(todo);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@Transactional
	public void update(@RequestBody Todo updatedTodo, @PathVariable("id") long id) throws IOException {
		if (id != updatedTodo.getId()) {
			repository.deleteById(id);
		}
		repository.save(updatedTodo);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") long id) {
		repository.deleteById(id);
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Todo get(@PathVariable("id") long id) {
		return  repository.findById(id).get();
	}

}
