start-docker-registry:
	- docker run -d -p 5000:5000 --name registry registry:2

build-docker-env:
	docker build -t localhost:5000/gradle dockers/gradle

build-docker-prod-image:
	docker build -t localhost:5000/rest_todo_prod .

deploy-production-local:
	- docker rm -f java_sample_prod
	- docker run -d --name rest_todo_prod -p 8800:8080 localhost:5000/rest_todo_prod
