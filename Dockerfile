FROM openjdk:8-jdk-alpine

COPY build/libs/app.war /app/

EXPOSE 8080
WORKDIR /app

#CMD /bin/bash -c 'java -jar spring-boot-sample-data-rest-0.1.0.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.war"]